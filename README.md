# NextCloud Talk Python api

## Overview

Python wrapper for NextCloud Talk api

This is based on the Python wrapper for NextCloud api at git@github.com:EnterpriseyIntranet/nextcloud-API.git  This library extends
it providing the same interface for the Talk api. It has been tested with Python 3.8, Nextcloud 19, and Talk 9.

## Installation

```sh
python3 setup.py install
```

## Usage

```python
from nextcloudtalk import NextCloudTalk
nct = NextCloudTalk(endpoint="https://nextcloud.your.domain", user="username", password="password")
r = nct.get_conversations()
print(r.data)
```