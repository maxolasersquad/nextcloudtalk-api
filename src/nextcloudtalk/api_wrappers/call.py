from nextcloud.base import WithRequester


class Call(WithRequester):
    API_URL = '/ocs/v2.php/apps/spreed/api/v1/call'

    def get_call_participants(self, token):
        return self.requester.get("{token}".format(token=token))

    def join_call(self, token, flags=None):
        return self.requester.post("{token}".format(token=token), {'flags': flags})

    def leave_call(self, token):
        return self.requester.delete("{token}".format(token=token))
