from .call import Call
from .chat import Chat
from .integration import Integration
from .participant import Participant
from .conversation import Conversation
from .signaling import Signaling
from .webinar import Webinar
from .settings import Settings

OCS_API_CLASSES = [Call, Chat, Conversation, Integration, Participant, Settings, Signaling, Webinar]
