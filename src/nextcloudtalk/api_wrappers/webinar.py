from nextcloud.base import WithRequester


class Webinar(WithRequester):
    API_URL = '/ocs/v2.php/apps/spreed/api/v1/room'

    def set_lobby_for_a_conversation(self, token, state, timer):
        url = "{token}/webinar/lobby".format(token=token)
        msg = {'state': state, 'timer': timer}
        return self.requester.put(url, msg)
