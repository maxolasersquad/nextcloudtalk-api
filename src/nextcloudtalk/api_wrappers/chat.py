from nextcloud.base import WithRequester


class Chat(WithRequester):
    API_URL = '/ocs/v2.php/apps/spreed/api/v1/chat'

    def get_chat_messages(
            self,
            token,
            look_into_future,
            limit=None,
            last_known_message_id=None,
            timeout=None,
            set_read_marker=None,
            include_last_known=None):
        url = "{token}".format(token=token)
        msg = {
            'lookIntoFuture': look_into_future,
            'limit': limit,
            'lastKnownMessageId': last_known_message_id,
            'timeout': timeout,
            'setReadMarker': set_read_marker,
            'includeLastKnown': include_last_known
        }
        return self.requester.get(url, msg)

    def send_new_chat_message(self, token, message, actor_display_name=None, reply_to=None, reference_id=None):
        url = "{token}".format(token=token)
        msg = {
            'message': message,
            'actorDisplayName': actor_display_name,
            'replyTo': reply_to,
            'referenceId': reference_id
        }
        return self.requester.post(url, msg)

    def mark_chat_as_read(self, token, last_read_message):
        url = "{token}/read".format(token=token)
        msg = {'lastReadMessage': last_read_message}
        return self.requester.post(url, msg)

    def get_mention_autocomplete_suggestions(self, token, search, limit=None, include_status=None):
        url = "{token}/mentions".format(token=token)
        msg = {
            'search': search,
            'limit': limit,
            'includeStatus': include_status
        }
        return self.requester.get(url, msg)
