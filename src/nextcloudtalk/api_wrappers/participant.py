from nextcloud.base import WithRequester


class Participant(WithRequester):
    API_URL = '/ocs/v2.php/apps/spreed/api/v1/room'

    def add_participant(self, token, new_participant, source):
        url = "{token}/participants".format(token=token)
        msg = {'newParticipant': new_participant, 'source': source}
        return self.requester.post(url, msg)

    def get_participants(self, token, include_status=True):
        url = "{token}/participants".format(token=token)
        msg = {'includeStatus': include_status}
        return self.requester.get(url, msg)

    def delete_participant(self, token, participant):
        url = "{token}/participants".format(token=token)
        msg = {'participant': participant}
        return self.requester.delete(url, msg)

    def remove_self(self, token):
        return self.requester.delete("{token}/participants/self".format(token=token))

    def remove_guest(self, token, participant):
        url = "{token}/participants/guests".format(token=token)
        msg = {'participant': participant}
        return self.requester.delete(url, msg)

    def join_conversation(self, token, force=False, password=None):
        url = "{token}/participants/active".format(token=token)
        msg = {"force": force}
        if password is not None:
            msg.password = password
        return self.requester.post(url, msg)

    def leave_conversation(self, token):
        return self.requester.delete("{token}/participants/active".format(token=token))

    def promote_to_moderator(self, token, participant, session_id):
        url = "{token}/moderators".format(token=token)
        msg = {'participant': participant, 'sessionId': session_id}
        return self.requester.post(url, msg)

    def demote_moderator(self, token, participant, session_id):
        url = "{token}/moderators".format(token=token)
        msg = {'participant': participant, 'sessionId': session_id}
        return self.requester.delete(url, msg)
