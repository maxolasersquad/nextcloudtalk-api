from nextcloud.base import WithRequester


class Signaling(WithRequester):
    API_URL = '/ocs/v2.php/apps/spreed/api/v1/signaling'

    def get_signaling_setting(self, token):
        return self.requester.get("settings", {token: token})
