from nextcloud.base import WithRequester


class Integration(WithRequester):
    API_URL = '/ocs/v2.php/apps/spreed/api/v1/'

    def get_conversation_for_internal_file(self, file_id):
        return self.requester.put("file/{file_id}".format(file_id=file_id))

    def get_conversation_for_public_share(self, share_token):
        return self.requester.put("publicshare/{share_token}".format(share_token=share_token))

    def create_conversation_to_request_the_password_for_public_share(self, share_token):
        return self.requester.post("publicshareauth", {"shareToken": share_token})
