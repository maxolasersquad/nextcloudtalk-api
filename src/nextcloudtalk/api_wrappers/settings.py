from nextcloud.base import WithRequester


class Settings(WithRequester):
    API_URL = '/ocs/v2.php/apps/spreed/api/v1/settings/usr'

    def set_setting(self, key, value):
        return self.requester.put("", {key: value})
