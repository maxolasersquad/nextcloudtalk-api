from nextcloud.base import WithRequester


class Conversation(WithRequester):
    API_URL = '/ocs/v2.php/apps/spreed/api/v1/room'

    def add_conversation(self, room_type, invite, source, name):
        msg = {'roomType': room_type, 'invite': invite, 'source': source, 'roomName': name}
        return self.requester.post("", msg)

    def get_conversations(self):
        return self.requester.get()

    def get_conversation(self, token):
        return self.requester.get("{token}".format(token=token))

    def rename_conversation(self, token, name):
        url = "{token}".format(token=token)
        msg = {'roomName': name}
        return self.requester.put(url, msg)

    def delete_conversation(self, token):
        return self.requester.delete("{token}".format(token=token))

    def make_conversation_public(self, token):
        return self.requester.post("{token}/public".format(token=token))

    def make_conversation_private(self, token):
        return self.requester.delete("{token}/public".format(token=token))

    def set_conversation_read_only(self, token):
        return self.requester.put("{token}/read-only".format(token=token))

    def set_conversation_password(self, token, password):
        return self.requester.put("{token}/password".format(token=token), {'password': password})

    def add_conversation_to_favorites(self, token):
        return self.requester.post("{token}/favorite".format(token=token))

    def remove_conversation_from_favorites(self, token):
        return self.requester.delete("{token}/favorite".format(token=token))

    def set_conversation_notification_level(self, token, level):
        url = "{token}/notify".format(token=token)
        msg = {'level': level}
        return self.requester.post(url, msg)
