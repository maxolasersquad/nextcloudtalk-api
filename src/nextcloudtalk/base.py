import enum


class ConversationTypes(enum.IntEnum):
    ONE_TO_ONE = 1
    GROUP = 2
    PUBLIC = 3
    CHANGELOG = 4


class ReadOnlyState(enum.IntEnum):
    READ_WRITE = 0
    READ_ONLY = 1


class ParticipantTypes(enum.IntEnum):
    OWNER = 1
    MODERATOR = 2
    USER = 3
    GUEST = 4
    USER_FOLLOWING_PUBLIC_LINK = 5
    GUEST_WITH_MODERATOR_PERMISSIONS = 6


class ParticipantInCallFlags(enum.IntEnum):
    DISCONNECTED = 0
    IN_CALL = 1
    PROVIDES_AUDIO = 2
    PROVIDES_VIDEO = 4


class ParticipantNotificationLevels(enum.IntEnum):
    DEFAULT = 0
    ALWAYS_NOTIFY = 1
    NOTIFY_ON_MENTION = 2
    NEVER_NOTIFY = 3


class Actors(enum.Enum):
    GUESTS = 'guests'
    USERS = 'users'
    BOTS = 'bot'


class WebinaryLobbyStates(enum.Enum):
    NO_LOBBY = 0
    LOBBY_FOR_NON_MODERATORS = 1


class SignalingModes(enum.Enum):
    INTERNAL = 'internal'
    EXTERNAL = 'external'
    CONVERSATION_CLUSTER = 'conversation_cluster'
