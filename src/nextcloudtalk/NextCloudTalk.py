from nextcloud import NextCloud

from nextcloud.requester import OCSRequester
from .api_wrappers import OCS_API_CLASSES


# noinspection PyMissingConstructor
class NextCloudTalk(NextCloud):

    def __init__(self, endpoint, user, password, json_output=True):
        ocs_requester = OCSRequester(endpoint, user, password, json_output)
        self.functionality_classes = [api_class(ocs_requester) for api_class in OCS_API_CLASSES]

        for functionality_class in self.functionality_classes:
            for potential_method in dir(functionality_class):
                if potential_method.startswith('_') or not callable(getattr(functionality_class, potential_method)):
                    continue
                setattr(self, potential_method, getattr(functionality_class, potential_method))
